﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DecisionMapper.Crud.Models;
using DecisionMapper.Crud.Repositories;

namespace DecisionMapper.Crud.Tests.Repositories
{
    public abstract class StubRepository<T> : IRepository<T> where T: class, IRepositoryEntity
    {
        private readonly ICollection<T> _collection;
        protected object locker = new object();

        protected StubRepository()
        {
            _collection = new List<T>();
        }

        public async Task<T> ReadAsync(int id)
        {
            return await Task.Run(() =>
            {
                return _collection.FirstOrDefault(x => x.Id == id);
            });
        }

        public async Task<ICollection<T>> ReadAsync()
        {
            return await Task.Run(() =>
            {
                return _collection;
            });
        }

        public async Task UpdateAsync(int id, T newEntity)
        {
            await Task.Run(() =>
            {
                lock(locker)
                {
                    var objToUpdate = _collection.FirstOrDefault(x => x.Id == id);

                    if (objToUpdate == null)
                    {
                        throw new KeyNotFoundException();
                    }

                    objToUpdate = newEntity ?? throw new Exception("Patch Entity is invalid!");
                }
            });
        }

        public async Task WriteAsync(T entity)
        {
            await Task.Run(() =>
            {
                lock(locker)
                {
                    if (_collection.Any(x => x.Id == entity.Id))
                    {
                        throw new Exception("Key already exists!");
                    }

                    _collection.Add(entity);
                }
            });
        }

        public async Task DeleteAsync(int id)
        {
            await Task.Run(() =>
            {
                lock(locker)
                {
                    var entityToDelete = _collection.FirstOrDefault(x => x.Id == id);

                    if (entityToDelete == null)
                    {
                        throw new KeyNotFoundException();
                    }

                    _collection.Remove(entityToDelete);
                }
            });
        }
    }
}
