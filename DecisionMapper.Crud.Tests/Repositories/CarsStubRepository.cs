﻿using System;
using System.Collections.Generic;
using DecisionMapper.Crud.Models;

namespace DecisionMapper.Crud.Tests.Repositories
{
    public class CarsStubRepository : StubRepository<Car>
    {
        public CarsStubRepository() { }
    }
}
