using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using System.Text;
using Microsoft.AspNetCore.TestHost;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Xunit;
using DecisionMapper.Crud.Models;
using DecisionMapper.Crud.Repositories;
using DecisionMapper.Crud.Tests.Repositories;

namespace DecisionMapper.Crud.Tests
{
    public class ApiTests
    {
        private readonly Car _testCar1 = new Car
        {
            Id = 1,
            Name = "Car1",
            Description = "Description of car1"
        };

        private readonly Car _testCar2 = new Car
        {
            Id = 2,
            Name = "Car2",
            Description = "Description of car 2"
        };

        [Fact(DisplayName = "GetAll test")]
        public async Task GetAllTest()
        {
            var client = CreateClient();

            var response = await client.PostAsJsonAsync("/api/cars", _testCar1);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);

            response = await client.GetAsync("/api/cars");
            var arrayOfCars = await response.Content.ReadAsAsync<Car[]>();
            Assert.True(arrayOfCars.Length > 0);
        }

        [Fact(DisplayName = "GetOne test")]
        public async Task GetOneTest()
        {
            var client = CreateClient();

            var response = await client.PostAsJsonAsync("/api/cars", _testCar1);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);

            response = await client.GetAsync($"/api/cars/{_testCar1.Id}");
            var car = await response.Content.ReadAsAsync<Car>();
            Assert.True(car.Id == _testCar1.Id);
        }

        [Fact(DisplayName = "Create test")]
        public async Task CreateTest()
        {
            var client = CreateClient();

            var response = await client.PostAsJsonAsync("/api/cars", _testCar1);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);

            response = await client.PostAsJsonAsync("/api/cars", _testCar2);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);

            response = await client.GetAsync("/api/cars");
            var cars = await response.Content.ReadAsAsync<Car[]>();
            Assert.True(cars.Length == 2);
        }

        [Fact(DisplayName = "Update test")]
        public async Task UpdateTest()
        {
            var client = CreateClient();

            var response = await client.PostAsJsonAsync("/api/cars", _testCar1);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);

            var patchObject = new
            {
                Id = _testCar1.Id,
                Name = "Changed car1 name"
            };
            
            response = await client.PatchAsync(
                $"/api/cars", 
                new StringContent(JsonConvert.SerializeObject(patchObject), Encoding.UTF8, "application/json"));
            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);

            response = await client.GetAsync($"/api/cars/{_testCar1.Id}");
            var responseCar = await response.Content.ReadAsAsync<Car>();
            Assert.Equal("Changed car1 name", responseCar.Name);

            var creationViaUpdateObj = new
            {
                Name = "Another car name"
            };

            response = await client.PatchAsync(
                $"/api/cars",
                new StringContent(JsonConvert.SerializeObject(creationViaUpdateObj), Encoding.UTF8, "application/json"));
            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);

            response = await client.GetAsync("/api/cars");
            var cars = await response.Content.ReadAsAsync<Car[]>();
            Assert.True(cars.Length == 2);
        }

        [Fact(DisplayName = "Update description to null test")]
        public async Task UpdateDescrToNullTest()
        {
            var client = CreateClient();

            var response = await client.PostAsJsonAsync("/api/cars", _testCar1);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);

            var patchObject = new descrUpdateObj
            {
                Id = _testCar1.Id,
                Description = null
            };

            response = await client.PatchAsync(
                $"/api/cars",
                new StringContent(JsonConvert.SerializeObject(patchObject), Encoding.UTF8, "application/json"));
            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);

            response = await client.GetAsync($"/api/cars/{_testCar1.Id}");
            var responseCar = await response.Content.ReadAsAsync<Car>();
            Assert.Equal(_testCar1.Name, responseCar.Name);
            Assert.Null(responseCar.Description);
        }

        [Fact(DisplayName = "Delete test")]
        public async Task DeleteTest()
        {
            var client = CreateClient();

            var response = await client.PostAsJsonAsync("/api/cars", _testCar1);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);

            response = await client.DeleteAsync($"/api/cars/{_testCar1.Id}");
            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);

            response = await client.GetAsync("/api/cars");
            var cars = await response.Content.ReadAsAsync<Car[]>();
            Assert.Empty(cars);
        }

        [Fact(DisplayName = "Not found test")]
        public async Task NotFoundTest()
        {
            var client = CreateClient();

            var response = await client.PatchAsync($"/api/cars", new StringContent("{ \"Id\": \"1\", \"Name\": \"Another name\" }", Encoding.UTF8, "application/json"));
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);

            response = await client.DeleteAsync($"/api/cars/{_testCar1.Id}");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact(DisplayName = "Already exists test")]
        public async Task AlreadyExistsTest()
        {
            var client = CreateClient();

            var response = await client.PostAsJsonAsync("/api/cars", _testCar1);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);

            response = await client.PostAsJsonAsync("/api/cars", _testCar1);
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact(DisplayName = "Bad create test")]
        public async Task BadCreateTest()
        {
            var client = CreateClient();

            var brokenCar = new
            {
                Name = "Broken car"
            };

            var response = await client.PostAsJsonAsync("/api/cars", brokenCar);
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);

            response = await client.GetAsync("/api/cars");
            var cars = await response.Content.ReadAsAsync<Car[]>();
            Assert.Empty(cars);
        }

        [Fact(DisplayName = "Bad update test")]
        public async Task BadUpdateTest()
        {
            var client = CreateClient();

            var response = await client.PostAsJsonAsync("/api/cars", _testCar1);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);

            var patchObject = new[]
            {
                new
                {
                    Wrong = "Wrong",
                    Fields = "Fields",
                    Set = "Set"
                }
            };

            response = await client.PatchAsync(
                $"/api/cars",
                new StringContent(JsonConvert.SerializeObject(patchObject), Encoding.UTF8, "application/json"));
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);

            response = await client.GetAsync("/api/cars");
            var cars = await response.Content.ReadAsAsync<Car[]>();
            Assert.True(cars.Length == 1);
        }

        private HttpClient CreateClient()
        {
            var server = new TestServer(new WebHostBuilder()
                             .ConfigureServices(services =>
                             {
                                 services.AddSingleton<IRepository<Car>, CarsStubRepository>();
                             })
                             .UseStartup<Startup>());

            return server.CreateClient();
        }
    }

    class descrUpdateObj : IRepositoryEntity
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
