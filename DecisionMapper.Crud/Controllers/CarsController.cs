﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SimplePatch;
using DecisionMapper.Crud.Models;
using DecisionMapper.Crud.Services;

namespace DecisionMapper.Crud.Controllers
{
    [Route("api/[controller]")]
    public class CarsController : Controller
    {
        IService<Car> _carsService;

        public CarsController(IService<Car> carsService)
        {
            _carsService = carsService;
        }

        // GET: api/cars
        [HttpGet]
        public async Task<IEnumerable<Car>> Get()
        {
            return await _carsService.GetAll();
        }

        // GET api/cars/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var car = await _carsService.Get(id);

            if (car == null)
            {
                return NotFound();
            }

            return Ok(car);
        }

        // POST api/cars
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Car car)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _carsService.Create(car);

                    // This is a bug in ASP.NET Core.
                    //return CreatedAtRoute("cars", new { id = car.Id }, car);

                    return CreatedAtAction("Create", car);
                }

                return BadRequest();
            }
            catch
            {
                return BadRequest();
            }
        }

        // PATCH api/cars/5
        [HttpPatch()]
        public async Task<IActionResult> Update([FromBody]Delta<Car> patch)
        {
            try
            {
                await _carsService.Update(patch);

                return NoContent();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _carsService.Delete(id);

                return NoContent();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
