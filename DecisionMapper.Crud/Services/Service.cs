﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using SimplePatch;
using DecisionMapper.Crud.Repositories;
using DecisionMapper.Crud.Models;

namespace DecisionMapper.Crud.Services
{
    public abstract class Service<T> : IService<T> where T: class, IRepositoryEntity, new()
    {
        private IRepository<T> _repository;

        protected Service(IRepository<T> repository)
        {
            this._repository = repository;
        }

        public async Task Create(T entity)
        {
            await _repository.WriteAsync(entity);
        }

        public async Task Delete(int id)
        {
            await _repository.DeleteAsync(id);
        }

        public async Task<T> Get(int id)
        {
            return await _repository.ReadAsync(id);
        }

        public async Task<ICollection<T>> GetAll()
        {
            return await _repository.ReadAsync();
        }

        public async Task Update(Delta<T> delta)
        {
            if (!delta.HasProperty("Id"))
            {
                var newEntity = new T();

                var maxId = (await _repository.ReadAsync()).Select(x => x.Id).Max();
                newEntity.Id = ++maxId;

                delta.Patch(newEntity);

                await _repository.WriteAsync(newEntity);

                return;
            }

            if(!(delta.TryGetValue("Id", out object idValue)))
            {
                throw new InvalidCastException();
            }

            if (!Int32.TryParse(idValue.ToString(), out int id))
            {
                throw new InvalidCastException();
            }

            var entityToUpdate = await _repository.ReadAsync(id);

            if (entityToUpdate == null)
            {
                throw new KeyNotFoundException();
            }

            delta.Patch(entityToUpdate);
            await _repository.UpdateAsync(id, entityToUpdate);
        }
    }
}
