﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using DecisionMapper.Crud.Models;
using SimplePatch;

namespace DecisionMapper.Crud.Services
{
    public interface IService<T> where T: class, IRepositoryEntity, new()
    {
        Task<T> Get(int id);
        Task<ICollection<T>> GetAll();
        Task Create(T entity);
        Task Update(Delta<T> delta);
        Task Delete(int id);
    }
}
