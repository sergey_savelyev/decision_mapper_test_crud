﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using DecisionMapper.Crud.Models;
using DecisionMapper.Crud.Repositories;

namespace DecisionMapper.Crud.Services
{
    public class CarsService : Service<Car>
    {
        public CarsService(IRepository<Car> repository) : base(repository)
        {

        }

        // Any additional methods for cars here :)
    }
}
