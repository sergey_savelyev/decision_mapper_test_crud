﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using MongoDB.Bson;
using DecisionMapper.Crud.Models;

namespace DecisionMapper.Crud.Repositories
{
    public class MongoRepository<T> : IRepository<T> where T: class, IRepositoryEntity
    {
        private IMongoDatabase _db;
        private string _collectionName;
        private const string ID_FIELD = "_id";

        private IMongoCollection<T> Collection => _db.GetCollection<T>(_collectionName);

        public MongoRepository(string collectionName, IConfiguration config)
        {
            var connectionString = config.GetSection("MongoDB")?.GetSection("ConnectionString")?.Value;
            var connection = new MongoUrlBuilder(connectionString);
            var client = new MongoClient(connectionString);

            _db = client.GetDatabase(connection.DatabaseName);
            _collectionName = collectionName;
        }

        public async Task DeleteAsync(int id)
        {
            await Collection.DeleteOneAsync(new BsonDocument(ID_FIELD, id));
        }

        public async Task<ICollection<T>> ReadAsync()
        {
            var builder = new FilterDefinitionBuilder<T>();
            return await Collection.Find(builder.Empty).ToListAsync();
        }

        public async Task<ICollection<T>> ReadAsync(FilterDefinition<T> filter)
        {
            return await Collection.Find(filter).ToListAsync();
        }

        public async Task<T> ReadAsync(int id)
        {
            return await Collection.Find(new BsonDocument(ID_FIELD, id)).FirstOrDefaultAsync();
        }

        public async Task UpdateAsync(int id, T newEntity)
        {
            await Collection.ReplaceOneAsync(new BsonDocument(ID_FIELD, id), newEntity);
        }

        public async Task WriteAsync(T entity)
        {
            await Collection.InsertOneAsync(entity);
        }
    }
}
