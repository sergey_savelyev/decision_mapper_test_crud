﻿using System;
using Microsoft.Extensions.Configuration;
using DecisionMapper.Crud.Models;

namespace DecisionMapper.Crud.Repositories
{
    public class CarsRepository : MongoRepository<Car>
    {
        public CarsRepository(IConfiguration config) : base("Cars", config)
        {
        }
    }
}
