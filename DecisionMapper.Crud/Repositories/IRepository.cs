﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DecisionMapper.Crud.Models;

namespace DecisionMapper.Crud.Repositories
{
    public interface IRepository<TEntity> where TEntity: class, IRepositoryEntity
    {
        Task<ICollection<TEntity>> ReadAsync();
        Task<TEntity> ReadAsync(int id);
        Task WriteAsync(TEntity entity);
        Task UpdateAsync(int id, TEntity newEntity);
        Task DeleteAsync(int id);
    }
}
