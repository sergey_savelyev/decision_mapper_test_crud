﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DecisionMapper.Crud.Models
{
    public class Car : IRepositoryEntity
    {
        public Car()
        {
            Name = string.Empty;
        }

        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
    }

}
