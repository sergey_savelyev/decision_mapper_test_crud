﻿using System;
namespace DecisionMapper.Crud.Models
{
    public interface IRepositoryEntity
    {
        int Id { get; set; }
    }
}
